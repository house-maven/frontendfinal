import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PropertyRoutingModule } from './property-routing.module';
import { PropertyListComponent } from './property-list/property-list.component';
import { AddPropertyComponent } from './add-property/add-property.component';
import { EditPropertyComponent } from './edit-property/edit-property.component';
import { PropertyMainComponent } from './property-main/property-main.component';
import { PropertyDetailsComponent } from './property-details/property-details.component';


@NgModule({
  declarations: [PropertyListComponent, AddPropertyComponent, EditPropertyComponent, PropertyMainComponent, PropertyDetailsComponent],
  imports: [
    CommonModule,
    PropertyRoutingModule,
    FormsModule,
    NgbModule
  ]
})
export class PropertyModule { }
