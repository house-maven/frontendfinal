import { PropertyMainComponent } from './property-main/property-main.component';
import { EditPropertyComponent } from './edit-property/edit-property.component';
import { AddPropertyComponent } from './add-property/add-property.component';
import { PropertyListComponent } from './property-list/property-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path : "",
    component : PropertyMainComponent,
    children : [
      {
        path : "property-list",
        component : PropertyListComponent
      },
      {
        path : "add-property",
        component : AddPropertyComponent
      },
      {
        path : "edit-property",
        component : EditPropertyComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PropertyRoutingModule { }
