import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  type = sessionStorage['type']

  constructor(private toastr : ToastrService,
    private router:Router) { }

  ngOnInit(): void {
  }

  onLogout()
  {
    this.toastr.success(`Logout ${sessionStorage['emailId']}`);
    sessionStorage.removeItem('token')
    sessionStorage.removeItem('emailId')
    sessionStorage.removeItem('firstName')
    sessionStorage.removeItem('lastName')
    sessionStorage.removeItem('type')
    this.router.navigate(['/auth/signin'])

  }

}
