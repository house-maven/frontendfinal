import { ProfileService } from './../profile.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  profileData = null;
  
  constructor(private router:Router,private ProfileService : ProfileService) { }

  ngOnInit(): void {
    this.loadProfile()
  }

   loadProfile()
   {
     this.ProfileService.getProfile().subscribe(res=>{
       if(res['status'] == "success")
       {
          this.profileData = res['data'][0]
       }
     })
   }
}
