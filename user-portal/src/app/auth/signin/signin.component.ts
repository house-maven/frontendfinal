import { UserService } from './../user.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  title = "Sigin"
  email = ''
  password = ''

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private userService: UserService 
  ) { }

  ngOnInit(): void {
  }

  onLogin() {
    if (this.email.length == 0) {
      this.toastr.error('please enter email')
    } else if (this.password.length == 0) {
      this.toastr.error('please enter password')
    } else {
      this.userService
        .login(this.email, this.password)
        .subscribe(response => {
          if (response['status'] == 'success') {
            const data = response['data'][0]
            console.log(data)

            // cache the user info
            sessionStorage['token'] = data['token']
            sessionStorage['firstName'] = data['firstName']
            sessionStorage['lastName'] = data['lastName']
            sessionStorage['type'] = data['type']
            sessionStorage['emailId'] = data['email']
            this.toastr.success(`Welcome ${data['firstName']} ${data['lastName']} to HomeMavens`)

            // goto the dashboard
            this.router.navigate(['/home/user/profile'])

          } else {
            this.toastr.error(response['error']);
          }
        })
    }
  }
}
