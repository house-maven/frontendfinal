import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PropertyRequirementRoutingModule } from './property-requirement-routing.module';
import { PropertyRequirementListComponent } from './property-requirement-list/property-requirement-list.component';
import { AddPropertyRequirementComponent } from './add-property-requirement/add-property-requirement.component';
import { EditPropertyRequirementComponent } from './edit-property-requirement/edit-property-requirement.component';
import { PropertyReqMainComponent } from './property-req-main/property-req-main.component';


@NgModule({
  declarations: [PropertyRequirementListComponent, AddPropertyRequirementComponent, EditPropertyRequirementComponent, PropertyReqMainComponent],
  imports: [
    CommonModule,
    PropertyRequirementRoutingModule,
    FormsModule
  ]
})
export class PropertyRequirementModule { }
