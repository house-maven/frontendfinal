import { Router } from '@angular/router';
import { PropertyReqService } from './../property-req.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-property-requirement-list',
  templateUrl: './property-requirement-list.component.html',
  styleUrls: ['./property-requirement-list.component.css']
})
export class PropertyRequirementListComponent implements OnInit {

  role = sessionStorage['type']
  property_data = []

  selectedProp ;
  constructor(private propertyReqService : PropertyReqService,private router:Router) { }

  ngOnInit(): void {
    this.loadProperty();
  }
  loadProperty()
  {
    this.propertyReqService.getPropertyRequirement().subscribe(res=>{
      console.log(res)
      if(res['status'] == "error")
      {
        console.log("Project Requirement Error!")
      }
      else{
        this.property_data = res['data']
      }
    })
  }
  onSelect(id){
    if(this.selectedProp != null){
      this.selectedProp = null;
    }else{
      this.selectedProp = id;
    }
  }

  editProp(id)
  {
    this.router.navigate(['/home/property-requirement/edit-property-requirement'],{queryParams:{id:id}})
  }
}
